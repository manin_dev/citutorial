<!DOCTYPE html>
<html>
<head>
  <title></title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bs/css/bootstrap.min.css'); ?>">
</head>
<body>

<div class="container">
 <?php echo form_open('users/edit_user') ; ?>
    <?php if (validation_errors()) : ?>
        <h3>Whoops! There was an error:</h3>
        <p><?php echo validation_errors(); ?></p>
    <?php endif; ?>

      <div class="form-group">
        <label>First Name</label>
         <?php echo form_input($first_name); ?>   
       </div>
      <div class="form-group">
        <label>Last Name</label>
         <?php echo form_input($last_name); ?>     
       </div>
      <div class="form-group">
          <label>Email</label>
         <?php echo form_input($email); ?>    
      </div>
      <div class="form-group">
          <label>Is Active</label>
          <?php echo form_checkbox($is_active); ?>     
      </div>
      <?php echo form_hidden($id); ?>

    <?php echo form_submit('submit', 'Update'); ?>
    or <?php echo anchor('users/index', 'cancel'); ?>
<?php echo form_close(); ?>
</div>
</body>
</html>
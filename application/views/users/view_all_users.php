<!DOCTYPE html>
<html>
<head>
  <title>View User</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bs/css/bootstrap.min.css'); ?>">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light bg-success">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
</nav>
<br/>
<?php echo anchor('users/new_user', 'Create User',array('class'=>'btn btn-success')); ?>
  <?php if ($query->num_rows() > 0 ) : ?> 

<table border="1" class="table">
  <tr>
      <td>ID</td>
      <td>First Name</td>
      <td>Last Name</td>
      <td>Email</td>
      <td>Created Date</td>
      <td>Is Active</td>
      <td colspan="2">Actions</td>
  </tr> 
  <?php foreach ($query->result() as $row) : ?>
  <tr>
      <td><?php echo $row->id; ?></td>
      <td><?php echo $row->first_name; ?></td>
      <td><?php echo $row->last_name; ?></td>
      <td><?php echo $row->email; ?></td>
      <td><?php echo date("d-m-Y", $row->created_date); ?></td>
      <td><?php echo ($row->is_active ? 'Yes' : 'No'); ?></td>
      <td><?php echo anchor('users/edit_user/'.$row->id, 'Edit') ; ?></td>
      <td><?php echo anchor('users/delete_user/'.$row->id, 'Delete') ; ?></td>
  </tr>           
  <?php endforeach ; ?>
</table>
<?php endif ; ?>
      </div>
    </div>
  </div>
</body>
</html>